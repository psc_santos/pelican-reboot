Title: Please Donate a Laptop Computer!
Date: 2022-04-10
Category: GitLab
Slug: pelican-on-gitlab-pages

Many people coming from Ukraine had to leave their computers back home and are currently in need of a good machine for school, work or to socialize.

I am gathering and refurbishing laptop computers, in order to distribute them among school children and their parents fleeing the Russian invasion in Ukraine.

# Here is how you can help:
Please place a laptop in a box (remember to fill it with enough packing material in order to keep it safe for transport) and send it by mail to this address:

````
Santos
Antonienstrasse 18
04229 - Leipzig
Germany
````

If possible, it would be of imense help if you'd add an external (wired) keyboard with Cyrillic letters ([such as this](https://www.amazon.de/Fujitsu-KB410-RU-Tastatur-schwarz/dp/B00IB7OP0A/ref=sr_1_4?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2FC5ZWJATA4XO&keywords=russian%2Bkeyboard&qid=1649518574&sprefix=russian%2Bkeyboard%2Caps%2C96&sr=8-4&th=1)) to your donation. This would be just awesome. Alternatively, please consider buying [these stickers](https://www.amazon.de/-/en/Russian-keyboard-stickers-laminated-transparent/dp/B002RSOQYY/ref=sr_1_4?crid=4SKZVAV8OVQP&keywords=russische+tastaturaufkleber&qid=1649619170&s=computers&sprefix=stickers+russi%2Ccomputers%2C142&sr=1-4) and adding them to your gift.

# Here is what I am looking for:

* Laptop Computers in good shape with a charging cable.

* Computer Keyboards with a Russian layout.

# Here is what I am NOT looking for:

* Smartphones

* Computers older than 10 years

* Desktop Computers

* Computer parts, monitors, hard drives, etc


# Here is what I will do:

* I will replace any software system present in the computer through a single fresh [Linux install](https://mxlinux.org/) in Ukrainian language.

* I will make sure the laptops reach people who need them.

# Saying "Thank You" & Data Confidentiality:

* The person receiving your laptop is likely to want to send you a message to say thank you. If you're happy with this, please place a piece of paper with your e-mail address or another type of contact in the laptop box. I will make sure the person who gets the machine also gets your note.

* I will format 100% of your Hard Disc. Recovery of any data will not be possible.

* I will not look for files and will not copy anything.

* Once you send in your computer, you kindly agree to say goodbye to everything it contains and agree not to ask me to try to recover data from any discs.

# [This is who I am](http://www.psc-santos.com/)
