#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Pablo Santos'
SITENAME = 'Computers for Ukrainian Families'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('If you prefer to donate money, I will use it for the keyboards and logistics:', '#'),
         ('My PayPal', 'https://www.paypal.com/pools/c/8ILrplZdrd'))

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
#https://tinyurl.com/33essvd4
